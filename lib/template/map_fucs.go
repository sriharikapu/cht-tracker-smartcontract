package template

import (
	"html/template"

	"gitlab.com/jmdatdenver/cht-tracker-smartcontract/lib/contx"
)

// FuncMaps to view
func FuncMaps() []template.FuncMap {
	return []template.FuncMap{
		map[string]interface{}{
			"Tr": contx.I18n,
		}}
}
