#env GOOS=linux GOARCH=amd64 go build
#docker build -t mercurius:cht-tracker-smartcontract .
#docker run -p 8080:8080 -d mercurius:cht-tracker-smartcontract

FROM scratch

ADD cht-tracker-smartcontract /
ADD conf/ /conf
ADD public/ /public
ADD locale/ /locale

CMD [ "/cht-tracker-smartcontract" ]