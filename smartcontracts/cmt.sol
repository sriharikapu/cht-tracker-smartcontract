pragma solidity 0.6.2;

import "./openzeppelin-contracts/contracts/token/ERC721/ERC721Full.sol";
import "./openzeppelin-contracts/contracts/drafts/Counters.sol";

contract Medicine is ERC721Full {
    using Counters for Counters.Counter;
    Counters.Counter private _medicineSKUs;

    constructor() public ERC721Full("ColoradoMedicineTracker", "NFT_CMT") {}

    function newProductItem(address factory, string memory ipfsHashMetadataProduction)
        public
        returns (uint256)
    {
        _medicineSKUs.increment();

        uint256 newItemId = _medicineSKUs.current();
        _mint(factory, newItemId);
        _setTokenURI(newItemId, ipfsHashMetadataProduction);

        return newItemId;
    }

    function registerMetadata(uint256 medicineSKU, string memory ipfsHashMetadata)
        public
    {
        require(
            _isApprovedOrOwner(_msgSender(), medicineSKU),
            "ERC721: transfer caller is not owner nor approved"
        );

        _setTokenURI(medicineSKU, ipfsHashMetadata);
    }
    function safeTransferOwner(
        address from,
        address to,
        uint256 medicineSKU,
        string memory ipfsHashMetadataTransportation
    ) public {
        require(
            _isApprovedOrOwner(_msgSender(), medicineSKU),
            "ERC721: transfer caller is not owner nor approved"
        );
        _transferFrom(from, to, medicineSKU);
        _setTokenURI(medicineSKU, ipfsHashMetadataTransportation);
    }

}
