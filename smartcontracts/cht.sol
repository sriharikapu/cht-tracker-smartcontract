pragma solidity 0.6.2;

import "./openzeppelin-contracts/contracts/token/ERC721/ERC721Full.sol";
import "./openzeppelin-contracts/contracts/drafts/Counters.sol";

contract HempCrop is ERC721Full {
    using Counters for Counters.Counter;
    Counters.Counter private _cropIds;
    event NewHarvestRecorded(uint256);

    constructor() public ERC721Full("ColoradoHempTracker", "NFT_CHT") {}

    function newHarvest(address grower, string memory ipfsHashNewHarvest)
        public
        returns (uint256)
    {
        _cropIds.increment();

        uint256 newItemId = _cropIds.current();
        _mint(grower, newItemId);
        _setTokenURI(newItemId, ipfsHashNewHarvest);
        emit NewHarvestRecorded(newItemId);
        return newItemId;
    }

    function registerSample(uint256 cropId, string memory sampleJsonIpfsHash)
        public
    {
        require(
            _isApprovedOrOwner(_msgSender(), cropId),
            "ERC721: transfer caller is not owner nor approved"
        );

        _setTokenURI(cropId, sampleJsonIpfsHash);
    }
    function safeTransferOwner(
        address from,
        address to,
        uint256 tokenId,
        string memory tokenURI
    ) public {
        require(
            _isApprovedOrOwner(_msgSender(), tokenId),
            "ERC721: transfer caller is not owner nor approved"
        );
        _transferFrom(from, to, tokenId);
        _setTokenURI(tokenId, tokenURI);
    }

}
